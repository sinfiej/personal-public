# personal-public

This project is only here to check the impact of enabling Enterprise Users and SSO in gitlab.com.

After one or other of these actions it's expected that this project should be forcably removed or similar. If not then we might need to do an investigation in to whether [this](https://docs.gitlab.com/ee/user/enterprise_user/#prevent-enterprise-users-from-creating-groups-and-projects-outside-the-corporate-group) hasn't worked or whether it only impacts new projects.
